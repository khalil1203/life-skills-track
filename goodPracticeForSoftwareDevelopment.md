# Good Practices for Software Development

## Question 1 : What is your one major takeaway from each one of the 6 sections. So 6 points in total.


### Gathering Requirements:
Major Takeaway: Effective communication and documentation of requirements are crucial for project success. Use tools for documentation and seek continuous feedback to ensure clarity and alignment.

### Over-Communication:
Major Takeaway: Communication is key, especially in remote work scenarios. Always keep your team informed about changes, challenges, and progress. Group communication channels are preferable, and prompt response is important.

### Asking Questions:
Major Takeaway: When seeking help or assistance, be clear and concise in explaining your problem. Provide context, solutions attempted, and utilize visuals/tools to aid understanding. Learn from open-source projects' issue reporting practices.

### Getting to Know Teammates:
Major Takeaway: Building relationships within your team enhances communication and collaboration. Make time for interactions, join meetings early, and be mindful of your teammates' schedules.

### Being Mindful of Teammates:
Major Takeaway: Respect your teammates' time and preferences for communication. Consolidate messages and choose appropriate communication methods. Respond promptly to maintain real-time conversations.

### 100% Involvement in Work:
Major Takeaway: Remote work demands focused attention and discipline. Practice deep work, manage distractions, and create a conducive work environment. Prioritize health, exercise, and proper nutrition.


## Question 2 - Which area do you think you need to improve on? What are your ideas to make progress in that area?


- If I struggles with effective communication, i could work on clearly articulating their thoughts, practicing active listening, and seeking feedback on their communication style.

- If i finds it challenging to focus during remote work, i could experiment with time management techniques like the Pomodoro Technique, create a distraction-free workspace, and set specific work hours to maintain a work-life balance.

- If i feels disconnected from their team, i could initiate informal conversations, schedule virtual coffee breaks, and actively participate in team-building activities. Building rapport can help improve the overall team dynamic.

- If i struggles with deep work and sustained attention, i could implement digital detox strategies, use website blockers during work hours, and establish a routine that includes breaks for physical activity and relaxation.
