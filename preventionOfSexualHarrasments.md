# Prevention of Sexual Harassment

## Question 1: What kinds of behavior cause sexual harassment?

Sexual harassment encompasses various forms of unwelcome conduct of a sexual nature. It can be categorized into three primary types: verbal, visual, and physical. Here are some specific behaviors that can contribute to sexual harassment:


### Verbal Harassment: 
This includes making comments about a person's clothing or body, sharing sexual or gender-based jokes or remarks, requesting sexual favors, repeatedly asking someone out, using sexual innuendos, spreading rumors about someone's personal or sexual life, or using foul and obscene language.


### Visual Harassment: 
Visual harassment involves displaying or sharing sexually suggestive or explicit content through posters, drawings, pictures, screen savers, cartoons, emails, or text messages.


### Physical Harassment: 
 It can involve sexual assault, impeding or blocking someone's movement, inappropriate touching such as kissing, hugging, patting, stroking, or rubbing, leering..


These behaviors, when severe or pervasive and affecting working conditions or creating a hostile work environment, can be considered sexual harassment.

## Question 2: What would you do in case you face or witness any incident or repeated incidents of such behavior?

It is important to take appropriate steps to address the situation. Here's what you can do:


### Document the incidents:
 Keep a detailed record of the incidents, including dates, times, locations, individuals involved, and a description of what happened. 


### Review company policies: 
Understand the reporting process and any available support mechanisms.


### Report the incidents: 
If you feel comfortable and safe doing so, report the incidents to your immediate supervisor, human resources department, or another designated authority within your organization. 

### Seek support: 
Reach out to trusted colleagues, friends, or family members to discuss your experiences and seek emotional support.


### Cooperate with investigations: 
Ensure that your perspective is heard and taken into account during the process.


### Know your rights: 
Consult labor laws, anti-discrimination laws, or employment regulations to understand your rights and potential avenues for recourse.

