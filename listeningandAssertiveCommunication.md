# Listening and Active Communication

## 1. Active Listening
### Question 1
***What are the steps/strategies to do Active Listening?***
### Answer 1
 Following are the steps/strategies for active listening:


***1. Fully Hear and Comprehend:***  Actively listen by fully hearing and comprehending the meaning of what the other person is saying. Pay attention to their words, tone, and non-verbal cues.


***2. Avoid Distractions:*** Minimize distractions by avoiding getting caught up in your own thoughts or external factors. Stay focused on the speaker and the topic being discussed.


***3. Avoid Interruptions:*** Practice restraint and refrain from interrupting the other person while they are speaking. Let them finish expressing their thoughts before responding.


***4. Use Door Openers:*** Utilize door openers, which are phrases that demonstrate your interest and encourage the other person to keep talking. 


***5. Show Listening through Body Language:*** Display attentive body language when appropriate. Maintain eye contact, nod in agreement or understanding, and use facial expressions to convey your engagement and interest in the conversation.


## 2. Reflective Listening
### Question 2
***According to Fisher's model, what are the key points of Reflective Listening?***
### Answer 2

***Reflective listening, according to Fisher's model, involves several key points:***


***1. Paying Attention:*** Give your full attention to the speaker and focus on what they are saying. Avoid distractions and try to be fully present in the conversation.


***2. Empathy:*** Try to understand and empathize with the speaker's feelings and emotions. Put yourself in their shoes and imagine how they might be experiencing the situation.


***3. Non-judgmental Attitude:*** Maintain an open and non-judgmental attitude towards the speaker's thoughts and opinions.


***4. Reflection:*** Reflect back the speaker's words, feelings, and underlying meaning to show that you understand and are actively listening. 


***5. Clarification:*** Seek clarification if there are any points or ideas that are unclear to you.


***6. Summarization:*** Summarize the main points of the speaker's message to demonstrate that you have actively listened and comprehended their thoughts. 


## 3. Reflection
### Question 3
 ***What are the obstacles in your listening process?***
### Answer 3
***The obstacles in my listening process are:***

***1. Technology Distractions:*** In today's digital age, the presence of smartphones, notifications, or multitasking with electronic devices can divert our attention away from active listening and impair our ability to fully engage in the conversation.

***2. Language or Cultural Barriers:*** Communication can be impeded by language differences or cultural nuances. If the speaker uses unfamiliar terms or expressions, or if there are cultural gaps, it can be more challenging to comprehend their message accurately.

### Question 4
 ***What can you do to improve your listening?***
### Answer 4
***I intend to counter the above mentioned obstacles as follows:***


***(A). Minimizing Technology Distractions:***


***1. Create a distraction-free environment:*** Silence or put away electronic devices, such as smartphones or laptops, during important conversations to avoid temptations and interruptions.


***2.Practice active presence:*** Be fully present in the conversation by giving my undivided attention to the speaker. Focus on their words, gestures, and non-verbal cues without checking your devices.




***(B) Overcoming Language or Cultural Barriers:***


***1. Develop cultural awareness:*** Educate myself about different cultures, their communication styles, and common customs. This knowledge will help me better understand and appreciate diverse perspectives.


***2. Ask for clarification:*** If I encounter unfamiliar terms or expressions, I politely ask the speaker to explain or provide examples to ensure I grasp their intended meaning accurately.





## 4. Types of Communication

***Passive Communication***

***Aggressive Communication***

***Passive Aggressive Communication***

***Aggressive Communication***



### Question 6
***When do you switch to Passive communication style in your day to day life?***
### Answer 5


***1. Non-Essential Matters:*** When the topic or issue at hand is relatively unimportant or inconsequential, using a passive style can save time and energy by not engaging in extensive discussions or debates.


***2. Disagreeing with Parents:*** When expressing disagreement with your parents, a passive communication style can help maintain a respectful and harmonious relationship. It allows you to express your differing opinion without being confrontational or disrespectful. By using a passive approach, you can choose your words carefully, express your perspective calmly, and show empathy towards their viewpoint, fostering a constructive dialogue while preserving the parent-child bond.


### Question 6
***When do you switch into Aggressive communication styles in your day to day life?***

### Answer 6

I don't usually communicate aggressively. But when someone practically refuses to listen to me while I am being assertive then I think it is justified to be aggressive if the matter is important. I remember being aggressive about a year ago when I was trying to discipline my younger brother about something important and he was being unreasonalbe. 


### Question 7
***When do you switch into Passive Aggressive (sarcasm/gossiping/taunts/silent treatment and others) communication styles in your day to day life?***

### Answer 7
I switch to Passive Agressive communication - specifically silent treatment - when I am sleepy and tired and the second person refuses to acknowledge that. When feeling sleepy, it is natural to experience a decrease in energy and attentiveness, which can impact effective communication. I think choosing to switch to a more silent mode during these times can be a practical approach to prevent misunderstandings and maintain clarity. By recognizing your fatigue and opting for silence, you can avoid potential miscommunications or errors that could arise from impaired focus or concentration.

### Question 8
***How can you make your communication assertive? You can analyse the videos  and then think what steps you can apply in your own life? (Watch the videos first before answering this question.)***

### Answer 8
***To make the communication more assertive, we can apply the following steps based on the video transcript:***


***1. Recognize and name your feelings:*** Instead of making judgments about others, focus on expressing your own feelings clearly and honestly. By identifying and naming your emotions, you can communicate them more effectively without blaming or attacking others.


***2. Realise your needs:*** Take the time to understand and articulate your needs. Look beyond the surface level and identify the underlying needs behind your requests.

***3. Start with low-stakes situations:*** Begin practicing assertive communication in situations that are less intimidating or have lower stakes. 


Assertive communication may not guarantee that you always get what you want, but it increases the likelihood of having your needs understood and respected in healthy relationships.